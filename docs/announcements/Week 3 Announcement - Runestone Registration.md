# Week 3 Announcement

I've set up a free course for the interactive eBook [Fundamentals of Python Programming](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html) for all coursework. You can use Cengage for reference or practice, but it is not required for you to get 100% on your homework. If you would like to cancel your Cengage membership, you will not need it for this course.

[Runestone.Academy](https://runestone.academy/) interactive ebooks are free and open source, and I've tested it out so I know it will work.

At offices hours Wed night, John Lawson and I signed him up and got started. Here's what you need to do:

1.  Browse to [Runestone.Academy](https://runestone.academy/)
2.  Click the blue \[Sign Up\] button at the top of the page
3.  Enter a username, the same e-mail addressyou used for this course, a password, and click the checkbox for Terms and Conditions.
4.  Enter the course name [sdccd\_mesa\_college\_cs179\_spring23](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html) in the text box near the bottom of the form
5.  Click submit or sign up and that should send you to the table of contents
6.  Make sure you're logged in and enrolled in the right course then click this link to make sure it works: [runestone.academy/ns/books/published/**sdccd\_mesa\_college\_cs179\_spring23**/index.html](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html "Link")
7.  Work your way through Chapter 1 and try to think of something to that you learned that you can share with others in the Discussions
8.  I'll send you a message if I can't find you in the runestone roster for the course before Sunday.

**Grades**

1.  Week 1: Anyone that participated in the **Discussion** for Week 1 will get 100%. Otherwise 85%.
2.  Week 2: Everyone gets 100%
3.  Week 3 ( **this Sunday at midnight (11:59 PM)** ) If you make any effort at all you'll get an 90% on your homework. 100% if you complete Chapter 1 in Fundamentals of Python Programming on **Runestone.Academy**